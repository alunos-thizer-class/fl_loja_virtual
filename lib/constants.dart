// Valores fixos da loja
const cepLoja = '01001000';
const valorMotoboy = 15.00;

const statusAguardandoPagamento = 'Aguardando Pagamento';
const statusVerificacaoPagamento = 'Verificação de Pagamento';

enum TipoPagto { deposito, boleto, cartao }
