// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lista_produto_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ListaProduto on _ListaProdutoBase, Store {
  final _$ordenacaoAtom = Atom(name: '_ListaProdutoBase.ordenacao');

  @override
  ListaProdutoOrder get ordenacao {
    _$ordenacaoAtom.reportRead();
    return super.ordenacao;
  }

  @override
  set ordenacao(ListaProdutoOrder value) {
    _$ordenacaoAtom.reportWrite(value, super.ordenacao, () {
      super.ordenacao = value;
    });
  }

  final _$loadingAtom = Atom(name: '_ListaProdutoBase.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$listaAtom = Atom(name: '_ListaProdutoBase.lista');

  @override
  List<ProdutoModel> get lista {
    _$listaAtom.reportRead();
    return super.lista;
  }

  @override
  set lista(List<ProdutoModel> value) {
    _$listaAtom.reportWrite(value, super.lista, () {
      super.lista = value;
    });
  }

  final _$favoritosAtom = Atom(name: '_ListaProdutoBase.favoritos');

  @override
  bool get favoritos {
    _$favoritosAtom.reportRead();
    return super.favoritos;
  }

  @override
  set favoritos(bool value) {
    _$favoritosAtom.reportWrite(value, super.favoritos, () {
      super.favoritos = value;
    });
  }

  final _$_ListaProdutoBaseActionController =
      ActionController(name: '_ListaProdutoBase');

  @override
  dynamic pesquisa(String value) {
    final _$actionInfo = _$_ListaProdutoBaseActionController.startAction(
        name: '_ListaProdutoBase.pesquisa');
    try {
      return super.pesquisa(value);
    } finally {
      _$_ListaProdutoBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic reordenar(ListaProdutoOrder value) {
    final _$actionInfo = _$_ListaProdutoBaseActionController.startAction(
        name: '_ListaProdutoBase.reordenar');
    try {
      return super.reordenar(value);
    } finally {
      _$_ListaProdutoBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic toggleFavoritos() {
    final _$actionInfo = _$_ListaProdutoBaseActionController.startAction(
        name: '_ListaProdutoBase.toggleFavoritos');
    try {
      return super.toggleFavoritos();
    } finally {
      _$_ListaProdutoBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
ordenacao: ${ordenacao},
loading: ${loading},
lista: ${lista},
favoritos: ${favoritos}
    ''';
  }
}
