import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/categoria_model.dart';
import 'package:fl_loja_virtual/model/favorito_model.dart';
import 'package:fl_loja_virtual/model/produto_model.dart';
import 'package:mobx/mobx.dart';

part 'lista_produto_store.g.dart';

enum ListaProdutoOrder { maiorValor, menorValor, aParaZ, zParaA }

class ListaProduto extends _ListaProdutoBase with _$ListaProduto {
  ListaProduto(
    CategoriaModel categoriaModel,
    String uid,
  ) : super(categoriaModel, uid);
}

abstract class _ListaProdutoBase with Store {
  //
  // Construtor
  _ListaProdutoBase(this.categoriaModel, this.uid) {
    _filtraLista();
  }

  final CategoriaModel categoriaModel;
  final String uid;

  String _termoPesquisa = '';
  List<FavoritoModel> _listaFavoritos;
  List<ProdutoModel> _listaProdutos;

  @observable
  ListaProdutoOrder ordenacao = ListaProdutoOrder.aParaZ;

  @observable
  bool loading = true;

  @observable
  List<ProdutoModel> lista = [];

  @observable
  bool favoritos = false;

  @action
  pesquisa(String value) {
    _termoPesquisa = value.toLowerCase();
    _filtraLista();
  }

  @action
  reordenar(ListaProdutoOrder value) {
    ordenacao = value;
    _filtraLista();
  }

  @action
  toggleFavoritos() {
    favoritos = !favoritos;
    _filtraLista();
  }

  _filtraLista() async {
    // A lista de produtos da categoria.
    // Carregada apenas uma vez
    if (_listaProdutos == null) {
      await _carregaListaProdutos();
    }

    // Lista de favoritos do usuario
    // Carregada apenas uma vez
    if (_listaFavoritos == null) {
      await _carregaListaFavoritos();
    }

    // Limpa a lista que sera
    // usada para mostrar na tela
    lista.clear();

    for (var item in _listaProdutos) {
      //
      // Se ha filtro de apenas favoritos
      // verificamos se o produto em questao
      // esta marcado como favorito
      // para este usuario
      if (favoritos) {
        var exists = false;
        for (var favorito in _listaFavoritos) {
          if (favorito.excluido == false &&
              favorito.fkProduto.docRef.path == item.docRef.path) {
            exists = true;
            break;
          }
        }

        if (!exists) {
          continue;
        }
      }

      // Filtro de pesquisa
      if (_termoPesquisa.isNotEmpty) {
        if (item.titulo.toLowerCase().contains(_termoPesquisa) ||
            item.detalhe.toLowerCase().contains(_termoPesquisa) ||
            item.chamada.toLowerCase().contains(_termoPesquisa)) {
          lista.add(item);
        }
      } else {
        lista.add(item);
      }
    }

    // Ordenacao dos produtos
    if (lista.isNotEmpty) {
      lista.sort((a, b) {
        var result = 0; // por padrao sao iguais

        switch (ordenacao) {
          case ListaProdutoOrder.aParaZ:
            result = (a.titulo.compareTo(b.titulo));
            break;
          case ListaProdutoOrder.zParaA:
            result = (b.titulo.compareTo(a.titulo));
            break;
          case ListaProdutoOrder.maiorValor:
            result = (b.preco.compareTo(a.preco));
            break;
          case ListaProdutoOrder.menorValor:
            result = (a.preco.compareTo(b.preco));
            break;
          default:
            throw "Ordenacao desconhecida";
        }

        return result;
      });
    }

    // garante que nao vai mostrar como carregando
    loading = false;
  }

  Future<void> _carregaListaProdutos() async {
    _listaProdutos = [];

    var produtoDocs = await Firestore.instance
        .collection('produto')
        .where('fk_categoria', isEqualTo: categoriaModel.docRef)
        .where('excluido', isEqualTo: false)
        .getDocuments();

    for (var prodDoc in produtoDocs.documents) {
      var item = ProdutoModel.fromDocument(prodDoc);
      _listaProdutos.add(item);
    }
  }

  Future<void> _carregaListaFavoritos() async {
    _listaFavoritos = [];

    var favoritoDocs = await Firestore.instance
        .collection('favorito')
        .where('uid', isEqualTo: uid)
        .getDocuments();

    for (var favDoc in favoritoDocs.documents) {
      var favorito = FavoritoModel.fromDocument(favDoc);
      await favorito.loadProduto(favDoc.data['fk_produto']);

      _listaFavoritos.add(favorito);
    }
  }
}
