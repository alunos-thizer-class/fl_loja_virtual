import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/constants.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_store.dart';
import 'package:fl_loja_virtual/model/compra_model.dart';
import 'package:fl_loja_virtual/model/endereco_model.dart';
import 'package:fl_loja_virtual/model/item_model.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:meta/meta.dart';

class FinalizaCompraController {
  CarrinhoStore carrinho;
  String uid;

  EnderecoModel _enderecoModel;

  FinalizaCompraController({
    @required this.carrinho,
    @required this.uid,
  })  : assert(carrinho != null),
        assert(uid != null);

  Future<bool> isValid() async {
    bool result = true;

    if (carrinho.items.isEmpty) {
      result = false;
    }

    if (carrinho.frete == null) {
      result = false;
    }

    // Busca endereco de entrega no banco de dados
    var endereco = await Firestore.instance
        .collection('endereco')
        .where('uid', isEqualTo: uid)
        .getDocuments();

    if (endereco.documents.isEmpty) {
      result = false;
    } else {
      // Tem endereco cadastrado
      _enderecoModel = EnderecoModel.fromDocument(endereco.documents.first);

      // verifica se pelo menos o cep esta corretamente cadastrado
      if (_enderecoModel.cep == null || _enderecoModel.cep.isEmpty) {
        result = false;
      }
    }

    return result;
  }

  Future<DocumentReference> save(TipoPagto tipoPagto) async {
    var seq = await Firestore.instance
        .collection('compra')
        .limit(1)
        .orderBy('sequence', descending: true)
        .getDocuments();

    // Busca o ultimo ID de compra registrado no banco
    // Note que a consulta nao leva em conta o UID
    var sequence = 1;
    if (seq.documents.isNotEmpty) {
      sequence = seq.documents.first.data['sequence'] + 1;
    }

    var compra = CompraModel(
      sequence: sequence,
      uid: uid,
      data: Timestamp.now(),
      status: statusAguardandoPagamento,
      tipoFrete: carrinho.frete.nome,
      prazoFrete: carrinho.frete.prazo,
      tipoPagamento: tipoPagto.value,
      valorFrete: carrinho.valorFrete,
      valorItens: carrinho.valorItems,
      valorTotal: carrinho.valorTotal,
      cep: _enderecoModel.cep,
      rua: _enderecoModel.rua,
      numero: _enderecoModel.numero,
      complemento: _enderecoModel.complemento,
      bairro: _enderecoModel.bairro,
      cidade: _enderecoModel.cidade,
      estado: _enderecoModel.estado,
    );

    // Insere a compra no banco
    await compra.insert();

    for (var item in carrinho.items) {
      var itemModel = ItemModel(
        titulo: item.produto.titulo,
        quantidade: item.quantidade,
        valorUnitario: item.produto.preco,
        valorTotal: item.valorItem,
        cor: item.cor,
        fkCompra: compra,
        fkProduto: item.produto,
      );

      await itemModel.insert();
    }

    return compra.docRef;
  }
}
