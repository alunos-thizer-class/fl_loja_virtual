import 'package:fl_loja_virtual/model/produto_model.dart';
import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';

part 'carrinho_item_store.g.dart';

class CarrinhoItemStore = _CarrinhoItemStoreBase with _$CarrinhoItemStore;

abstract class _CarrinhoItemStoreBase with Store {
  _CarrinhoItemStoreBase({
    @required this.produto,
    this.cor,
    this.quantidade = 1,
  });

  @observable
  ProdutoModel produto;

  @observable
  int quantidade;

  @observable
  String cor;

  @computed
  double get valorItem => produto.preco * quantidade;

  @action
  increment() {
    quantidade++;
  }

  @action
  decrement() {
    quantidade--;
    if (quantidade < 1) {
      quantidade = 1;
    }
  }
}
