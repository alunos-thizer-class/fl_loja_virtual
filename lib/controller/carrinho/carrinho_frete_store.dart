import 'package:mobx/mobx.dart';

part 'carrinho_frete_store.g.dart';

class CarrinhoFreteStore = _CarrinhoFreteStoreBase with _$CarrinhoFreteStore;

abstract class _CarrinhoFreteStoreBase with Store {
  _CarrinhoFreteStoreBase({this.nome, this.valor, this.prazo});

  @observable
  String nome;

  @observable
  double valor;

  @observable
  int prazo;
}
