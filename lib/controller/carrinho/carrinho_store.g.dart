// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'carrinho_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CarrinhoStore on _CarrinhoStoreBase, Store {
  Computed<double> _$valorItemsComputed;

  @override
  double get valorItems =>
      (_$valorItemsComputed ??= Computed<double>(() => super.valorItems,
              name: '_CarrinhoStoreBase.valorItems'))
          .value;
  Computed<double> _$valorTotalComputed;

  @override
  double get valorTotal =>
      (_$valorTotalComputed ??= Computed<double>(() => super.valorTotal,
              name: '_CarrinhoStoreBase.valorTotal'))
          .value;
  Computed<double> _$valorFreteComputed;

  @override
  double get valorFrete =>
      (_$valorFreteComputed ??= Computed<double>(() => super.valorFrete,
              name: '_CarrinhoStoreBase.valorFrete'))
          .value;

  final _$itemsAtom = Atom(name: '_CarrinhoStoreBase.items');

  @override
  ObservableList<CarrinhoItemStore> get items {
    _$itemsAtom.reportRead();
    return super.items;
  }

  @override
  set items(ObservableList<CarrinhoItemStore> value) {
    _$itemsAtom.reportWrite(value, super.items, () {
      super.items = value;
    });
  }

  final _$freteAtom = Atom(name: '_CarrinhoStoreBase.frete');

  @override
  CarrinhoFreteStore get frete {
    _$freteAtom.reportRead();
    return super.frete;
  }

  @override
  set frete(CarrinhoFreteStore value) {
    _$freteAtom.reportWrite(value, super.frete, () {
      super.frete = value;
    });
  }

  final _$_CarrinhoStoreBaseActionController =
      ActionController(name: '_CarrinhoStoreBase');

  @override
  dynamic addItem(CarrinhoItemStore item) {
    final _$actionInfo = _$_CarrinhoStoreBaseActionController.startAction(
        name: '_CarrinhoStoreBase.addItem');
    try {
      return super.addItem(item);
    } finally {
      _$_CarrinhoStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic remove(CarrinhoItemStore item) {
    final _$actionInfo = _$_CarrinhoStoreBaseActionController.startAction(
        name: '_CarrinhoStoreBase.remove');
    try {
      return super.remove(item);
    } finally {
      _$_CarrinhoStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic limpar() {
    final _$actionInfo = _$_CarrinhoStoreBaseActionController.startAction(
        name: '_CarrinhoStoreBase.limpar');
    try {
      return super.limpar();
    } finally {
      _$_CarrinhoStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
items: ${items},
frete: ${frete},
valorItems: ${valorItems},
valorTotal: ${valorTotal},
valorFrete: ${valorFrete}
    ''';
  }
}
