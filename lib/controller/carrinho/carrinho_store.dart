import 'package:fl_loja_virtual/controller/carrinho/carrinho_item_store.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_frete_store.dart';
import 'package:mobx/mobx.dart';

part 'carrinho_store.g.dart';

class CarrinhoStore = _CarrinhoStoreBase with _$CarrinhoStore;

abstract class _CarrinhoStoreBase with Store {
  @observable
  var items = ObservableList<CarrinhoItemStore>();

  @observable
  CarrinhoFreteStore frete;

  @computed
  double get valorItems {
    double result = 0.0;

    for (var i in items) {
      result += i.valorItem;
    }
    return result;
  }

  @computed
  double get valorTotal {
    double result = valorItems;

    if (frete != null) {
      result += frete.valor;
    }
    return result;
  }

  @computed
  double get valorFrete => frete?.valor ?? 0.0;

  @action
  addItem(CarrinhoItemStore item) {
    CarrinhoItemStore exists = _getItem(item);

    if (exists != null) {
      exists.quantidade++;
    } else {
      items.add(item);
    }

    // cliente tem que selecionar novamente
    frete = null;
  }

  @action
  remove(CarrinhoItemStore item) {
    items.remove(item);

    // cliente tem que selecionar novamente
    frete = null;
  }

  @action
  limpar() {
    items.clear();
    frete = null;
  }

  CarrinhoItemStore _getItem(CarrinhoItemStore item) {
    CarrinhoItemStore exists;
    for (var i in items) {
      if (i.produto.docRef.documentID == item.produto.docRef.documentID &&
          i.cor == item.cor) {
        exists = i;
        break;
      }
    }
    return exists;
  }
}
