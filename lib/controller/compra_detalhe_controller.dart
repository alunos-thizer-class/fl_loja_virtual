import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/compra_model.dart';
import 'package:fl_loja_virtual/model/item_model.dart';

/// Este "controller" na verdade so existe para
/// que ao buscar os dados da compra e dos itens
/// nos nao tenhamos que usar um Map do tipo:
///
/// ```dart
/// // ...
/// Text(mapData['compra']['valor_total']);
/// // ...
/// ```
class CompraDetalheController {
  CompraModel compra;
  List<ItemModel> items;

  /// Este metodo vai de fato fazer as consultas no Firestore,
  /// depois criar os objetos de [CompraModel] e a lista de [ItemModel]
  /// e por fim retornar a propria instancia
  Future<CompraDetalheController> loadCompraComItens(
    DocumentReference compraRef,
  ) async {
    compra = CompraModel.fromDocument(await compraRef.get());

    // Realiza a consulta pela lista de items da compra
    var query = await Firestore.instance
        .collection('item')
        .where('fk_compra', isEqualTo: compraRef)
        .where('excluido', isEqualTo: false)
        .getDocuments();

    items = <ItemModel>[];
    for (var doc in query.documents) {
      // Popula uma model para cada item da compra
      var item = ItemModel.fromDocument(doc);

      // Carrega informacoes do produto
      await item.loadProduto(doc['fk_produto']);

      items.add(item);
    }

    return this;
  }
}
