import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/abstract_model.dart';
import 'package:fl_loja_virtual/model/categoria_model.dart';

class ProdutoModel extends AbstractModel {
  @override
  String get path => 'produto';

  @override
  DocumentReference docRef;

  @override
  bool excluido;

  String titulo;
  String chamada;
  String detalhe;
  double preco;
  CategoriaModel fkCategoria;
  String imagem;
  bool destaque;

  ProdutoModel({
    this.titulo,
    this.chamada,
    this.detalhe,
    this.preco,
    this.fkCategoria,
    this.imagem,
    this.destaque,
    this.excluido = false,
  });

  ProdutoModel.fromJson(this.docRef, Map<String, dynamic> json)
      : titulo = json['titulo'].toString(),
        chamada = json['chamada'].toString(),
        detalhe = json['detalhe'].toString(),
        preco = double.parse(json['preco'].toString()),
        fkCategoria = (json['fk_categoria'] is CategoriaModel)
            ? json['fk_categoria']
            : null,
        imagem = json['imagem'],
        destaque = json['destaque'],
        excluido = json['excluido'];

  ProdutoModel.fromDocument(DocumentSnapshot doc)
      : this.fromJson(doc.reference, doc.data);

  @override
  Map<String, dynamic> toJson({bool fullJson = false}) => {
        'titulo': titulo,
        'chamada': chamada,
        'detalhe': detalhe,
        'preco': preco,
        'fk_categoria':
            fullJson ? fkCategoria : referenceFromModel(fkCategoria),
        'imagem': imagem,
        'destaque': destaque,
        'excluido': excluido,
      };

  static Future<ProdutoModel> get(String documentPath, {full = false}) async {
    var item = await Firestore.instance.document(documentPath).get();
    var result = ProdutoModel.fromJson(item.reference, item.data);

    if (full && item.data['fk_categoria'] is DocumentReference) {
      await result.loadCategoria(item.data['fk_categoria']);
    }

    return result;
  }

  Future<CategoriaModel> loadCategoria(DocumentReference itemRef) async {
    var categoria = await itemRef.get();
    fkCategoria = categoria.exists
        ? CategoriaModel.fromJson(itemRef, categoria.data)
        : null;
    return fkCategoria;
  }
}
