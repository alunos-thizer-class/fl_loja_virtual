import 'package:cloud_firestore/cloud_firestore.dart';

abstract class AbstractModel {
  /// O caminho base da collection que extende a esta
  /// classe.
  ///
  /// Para pegar do Firestore todos os documentos desta
  /// collection basta: Firestore.instance.collection(path).getDocuments()...
  String get path;

  /// Id de referencia do documento no Firestore
  DocumentReference docRef;

  /// Todos os documentos de todas as collections
  /// devem ter um campo que vai informar se o
  /// registro ja foi excluido
  bool excluido = false;

  /// Este metodo deve ser implementado para transformar
  /// a instancia da classe em json
  Map<String, dynamic> toJson({bool fullJson = false});

  /// Apos popular uma nova instancia da classe que
  /// extende a essa eh possivel inserir este registro
  /// no Firestore chamando este metodo.
  ///
  /// Este metodo usara o resultado de [toJson()]
  ///
  /// [docRef] neste caso deve obrigatoriamente ser null
  Future<void> insert() async {
    if (docRef != null) {
      throw "Isso exige uma nova instancia de '${super.runtimeType}'";
    }
    docRef = await Firestore.instance.collection(path).add(toJson());
  }

  /// Se [docRef] nao for null este metodo vai automaticamente
  /// fazer um update no Firestore usando o resultado de [toJson()]
  Future<void> update() async {
    if (docRef == null) {
      throw "'${super.runtimeType}'.docRef não pode ser null para um update";
    }

    var data = toJson();

    // Remove da "lista" todos os registros do tipo chave estrangeira
    // ou seja, que comecam com "fk_" e que estao vazios
    data.removeWhere((key, value) =>
        key.startsWith('fk_') && (value is String) && value.isEmpty);

    await Firestore.instance
        .document('$path/${docRef.documentID}')
        .updateData(data);
  }

  /// Este metodo fara com que o documento seja removido
  /// no Firestore.
  ///
  /// Normalmente esse delete não sera real, mas apenas
  /// marcara o registro com o campo [excluido] como true.
  ///
  /// Utilize o [logico] como false para excliur de fato o
  /// registro do banco.
  Future<void> delete({bool logico = true}) async {
    if (docRef == null) {
      throw "'${super.runtimeType}'.docRef não pode ser null para um delete";
    }

    if (logico) {
      excluido = true; // marca como excluido
      update(); // roda update
    } else {
      await Firestore.instance.document('$path/${docRef.documentID}').delete();
    }
  }

  /// Este metodo vai verificar se [model] eh nulo
  /// e caso nao seja vai retornar o valor de [docRef]
  /// de dentro de sua instancia. Caso contrario retornara
  /// apenas uma string vazia.
  ///
  /// Retorno: [DocumentReference] ou [String] vazia
  referenceFromModel(AbstractModel model) {
    return model?.docRef ?? '';
  }

  @override
  String toString() {
    return toJson(fullJson: true).toString();
  }
}
