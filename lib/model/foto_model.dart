import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/abstract_model.dart';
import 'package:fl_loja_virtual/model/produto_model.dart';

class FotoModel extends AbstractModel {
  @override
  String get path => 'foto';

  @override
  DocumentReference docRef;

  @override
  bool excluido;

  String url;
  ProdutoModel fkProduto;

  FotoModel({
    this.fkProduto,
    this.url,
    this.excluido = false,
  });

  FotoModel.fromJson(this.docRef, Map<String, dynamic> json)
      : url = json['url'],
        fkProduto =
            (json['fk_produto'] is ProdutoModel) ? json['fk_produto'] : null,
        excluido = json['excluido'];

  FotoModel.fromDocument(DocumentSnapshot doc)
      : this.fromJson(doc.reference, doc.data);

  @override
  Map<String, dynamic> toJson({bool fullJson = false}) => {
        'url': url,
        'fk_produto': fullJson ? fkProduto : referenceFromModel(fkProduto),
        'excluido': excluido,
      };

  static Future<FotoModel> get(String documentPath, {full = false}) async {
    var item = await Firestore.instance.document(documentPath).get();
    var result = FotoModel.fromJson(item.reference, item.data);

    if (full && item.data['fk_produto'] is DocumentReference) {
      await result.loadProduto(item.data['fk_produto']);
    }

    return result;
  }

  Future<ProdutoModel> loadProduto(DocumentReference itemRef) async {
    var produto = await itemRef.get();
    fkProduto =
        produto.exists ? ProdutoModel.fromJson(itemRef, produto.data) : null;
    return fkProduto;
  }
}
