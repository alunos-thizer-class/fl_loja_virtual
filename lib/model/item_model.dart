import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/abstract_model.dart';
import 'package:fl_loja_virtual/model/compra_model.dart';
import 'package:fl_loja_virtual/model/produto_model.dart';

class ItemModel extends AbstractModel {
  @override
  String get path => 'item';

  @override
  DocumentReference docRef;

  @override
  bool excluido;

  String titulo;
  int quantidade;
  double valorUnitario;
  double valorTotal;
  String cor;

  CompraModel fkCompra;
  ProdutoModel fkProduto;

  ItemModel({
    this.titulo,
    this.quantidade,
    this.valorUnitario,
    this.valorTotal,
    this.cor,
    this.fkCompra,
    this.fkProduto,
    this.excluido = false,
  });

  ItemModel.fromJson(this.docRef, Map<String, dynamic> json)
      : titulo = json['titulo'],
        quantidade = json['quantidade'],
        valorUnitario = double.parse(json['valor_unitario'].toString()),
        valorTotal = double.parse(json['valor_total'].toString()),
        cor = json['cor'],
        fkCompra =
            (json['fk_compra'] is CompraModel) ? json['fk_compra'] : null,
        fkProduto =
            (json['fk_produto'] is ProdutoModel) ? json['fk_produto'] : null,
        excluido = json['excluido'];

  ItemModel.fromDocument(DocumentSnapshot doc)
      : this.fromJson(doc.reference, doc.data);

  @override
  Map<String, dynamic> toJson({bool fullJson = false}) => {
        'titulo': titulo,
        'quantidade': quantidade,
        'valor_unitario': valorUnitario,
        'valor_total': valorTotal,
        'cor': cor,
        'fk_compra': fullJson ? fkCompra : referenceFromModel(fkCompra),
        'fk_produto': fullJson ? fkProduto : referenceFromModel(fkProduto),
        'excluido': excluido,
      };

  static Future<ItemModel> get(String documentPath, {full = false}) async {
    var item = await Firestore.instance.document(documentPath).get();
    var result = ItemModel.fromJson(item.reference, item.data);

    if (full) {
      if (item.data['fk_compra'] is DocumentReference) {
        await result.loadCompra(item.data['fk_compra']);
      }

      if (item.data['fk_produto'] is DocumentReference) {
        await result.loadProduto(item.data['fk_produto']);
      }
    }

    return result;
  }

  Future<CompraModel> loadCompra(DocumentReference itemRef) async {
    var compra = await itemRef.get();
    fkCompra =
        compra.exists ? CompraModel.fromJson(itemRef, compra.data) : null;
    return fkCompra;
  }

  Future<ProdutoModel> loadProduto(DocumentReference itemRef) async {
    var produto = await itemRef.get();
    fkProduto =
        produto.exists ? ProdutoModel.fromJson(itemRef, produto.data) : null;
    return fkProduto;
  }
}
