import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/abstract_model.dart';

class EnderecoModel extends AbstractModel {
  @override
  String get path => 'endereco';

  @override
  DocumentReference docRef;

  @override
  bool excluido;

  String uid; // Id do usuario no Firebase
  String cep;
  String rua;
  String numero;
  String complemento;
  String bairro;
  String cidade;
  String estado;

  EnderecoModel({
    this.uid,
    this.cep = '',
    this.rua = '',
    this.numero = '',
    this.complemento = '',
    this.bairro = '',
    this.cidade = '',
    this.estado = '',
    this.excluido = false,
  });

  EnderecoModel.fromJson(this.docRef, Map<String, dynamic> json)
      : uid = json['uid'],
        cep = json['cep'],
        rua = json['rua'],
        numero = json['numero'],
        complemento = json['complemento'],
        bairro = json['bairro'],
        cidade = json['cidade'],
        estado = json['estado'],
        excluido = json['excluido'];

  EnderecoModel.fromDocument(DocumentSnapshot doc)
      : this.fromJson(doc.reference, doc.data);

  @override
  Map<String, dynamic> toJson({bool fullJson = false}) => {
        'uid': uid,
        'cep': cep,
        'rua': rua,
        'numero': numero,
        'complemento': complemento,
        'bairro': bairro,
        'estado': estado,
        'cidade': cidade,
        'excluido': excluido,
      };

  static Future<EnderecoModel> get(String documentPath, {full = false}) async {
    var item = await Firestore.instance.document(documentPath).get();
    return EnderecoModel.fromJson(item.reference, item.data);
  }
}
