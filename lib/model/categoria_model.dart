import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/abstract_model.dart';
import 'package:flutter/src/widgets/icon_data.dart';

class CategoriaModel extends AbstractModel {
  @override
  String get path => 'categoria';

  @override
  DocumentReference docRef;

  @override
  bool excluido;

  String nome;
  String icone;

  CategoriaModel({
    this.nome,
    this.icone,
    this.excluido = false,
  });

  CategoriaModel.fromJson(this.docRef, Map<String, dynamic> json)
      : nome = json['nome'],
        icone = json['icone'],
        excluido = json['excluido'];

  CategoriaModel.fromDocument(DocumentSnapshot doc)
      : this.fromJson(doc.reference, doc.data);

  @override
  Map<String, dynamic> toJson({bool fullJson = false}) => {
        'nome': nome,
        'icone': icone,
        'excluido': excluido,
      };

  static Future<CategoriaModel> get(String documentPath, {full = false}) async {
    var item = await Firestore.instance.document(documentPath).get();
    return CategoriaModel.fromJson(item.reference, item.data);
  }

  /// No Firestore foi salvo apenas o codigo como String da
  /// seguinte forma: 0xe8b6
  ///
  /// Este metodo pega este valor, transforma para int e
  /// aplica isso no [IconData] que com o [fontFamily] vai
  /// gerar um icone.
  IconData getIcone([String fontFamily = 'MaterialIcons']) {
    return IconData(int.parse(icone), fontFamily: fontFamily);
  }
}
