import 'package:fl_loja_virtual/controller/carrinho/carrinho_store.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/view/carrinho/carrinho_page.dart';
import 'package:fl_loja_virtual/view/carrinho/finaliza_page.dart';
import 'package:fl_loja_virtual/view/compras/compras_page.dart';
import 'package:fl_loja_virtual/view/favoritos/favoritos_page.dart';
import 'package:fl_loja_virtual/view/login/cadastro_page.dart';
import 'package:fl_loja_virtual/view/login/login_page.dart';
import 'package:fl_loja_virtual/view/login/login_recuperar_page.dart';
import 'package:fl_loja_virtual/view/login/splash_page.dart';
import 'package:fl_loja_virtual/view/perfil/perfil_page.dart';
import 'package:flutter/material.dart';

import 'package:fl_loja_virtual/view/home/home_page.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import 'view/layout.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //
    // Impede que o aplicativo possa ser
    // usado com o aparelho de lado
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      // DeviceOrientation.landscapeLeft,
      // DeviceOrientation.landscapeRight,
    ]);

    return MultiProvider(
      providers: [
        Provider<UserController>(create: (_) => UserController()),
        Provider<CarrinhoStore>(create: (_) => CarrinhoStore()),
      ],
      child: GetMaterialApp(
        enableLog: false,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'JosefinSans',
          primarySwatch: Colors.blue,
          accentColor: Layout.light(),
          textTheme: TextTheme(
            headline6: TextStyle(shadows: [
              BoxShadow(
                spreadRadius: 2,
                blurRadius: 2,
                color: Layout.dark(.3),
                offset: Offset(1, 2),
              ),
            ]),
          ),
        ),
        initialRoute: SplashPage.tag,
        routes: {
          SplashPage.tag: (context) => SplashPage(),
          LoginPage.tag: (context) => LoginPage(),
          LoginRecuperarPage.tag: (context) => LoginRecuperarPage(),
          CadastroPage.tag: (context) => CadastroPage(),
          HomePage.tag: (context) => HomePage(),
          FavoritosPage.tag: (context) => FavoritosPage(),
          PerfilPage.tag: (context) => PerfilPage(),
          CarrinhoPage.tag: (context) => CarrinhoPage(),
          FinalizaPage.tag: (context) => FinalizaPage(),
          ComprasPage.tag: (context) => ComprasPage(),
        },
      ),
    );
  }
}
