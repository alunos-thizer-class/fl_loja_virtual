import 'package:cloud_firestore_platform_interface/src/timestamp.dart';
import 'package:fl_loja_virtual/constants.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

//
// Para os extension methods funcionar é necessário
// que exista um arquivo com o nome 'analysis_options.yaml'
// na raiz do projeto com o seguinte conteúdo:
//
// enable-experiment:
//   - extension-methods
//
// e que no arquivo 'pubspec.yaml'
// o envronment skd seja: ">=2.6.0 <3.0.0"
//

extension TipoPagtoToString on TipoPagto {
  /// Traduz a enum para uma string compatível com ele.
  /// Por exemplo: para [TipoPagto.boleto] o retorno será 'boleto'
  String get value => this.toString().split('.')[1];
}

/// Recebe uma string e se for compatível com algum
/// dos valores em [TipoPagto] retorna em formato enum
TipoPagto stringToTipoPagto(String tipo) {
  return TipoPagto.values.firstWhere(
    (element) => element.value == tipo,
    orElse: () => null,
  );
}

/// Informa algo ao usuario
void snackBarInfo(String message) {
  Get.snackbar(
    'Importante!',
    message,
    backgroundColor: Colors.blue[900],
    margin: const EdgeInsets.all(20),
    dismissDirection: SnackDismissDirection.HORIZONTAL,
    colorText: Layout.light(),
  );
}

/// Mostra mensagem de sucesso ao usuario
void snackBarSuccess(String message) {
  Get.snackbar(
    'Feito!',
    message,
    backgroundColor: Colors.green,
    margin: const EdgeInsets.all(20),
    dismissDirection: SnackDismissDirection.HORIZONTAL,
    colorText: Layout.light(),
  );
}

/// Mostra mensagem de erro para usuario
void snackBarDanger(String message) {
  Get.snackbar(
    'Ooops!',
    message,
    backgroundColor: Colors.red,
    margin: const EdgeInsets.all(20),
    dismissDirection: SnackDismissDirection.HORIZONTAL,
    colorText: Layout.light(),
  );
}

/// Mostra um snackbar para mensagens que nao
/// sao necessariamente erros, mas pode gerar.
/// Geralmente exige acao do usuario
void snackBarWarning(String message) {
  Get.snackbar(
    'Atenção!',
    message,
    backgroundColor: Layout.secondaryHight(),
    margin: const EdgeInsets.all(20),
    dismissDirection: SnackDismissDirection.HORIZONTAL,
    colorText: Layout.light(),
  );
}

extension extrasToDouble on double {
  /// Este metodo vai facilitar a transformacao
  /// de um valor em double para o formato de
  /// moeda usado no Brasil, ou seja, BRL
  String toBRL() {
    String result = 'R\$ ';

    result += this.toStringAsFixed(2);
    result = result.replaceFirst('\.', ',');

    return result;
  }
}

extension extrasToTimestamp on Timestamp {
  /// Este metodo vai formatar sua data
  /// que chegou do Firestore como Timestamp
  /// para um formato mais amigavel, normalmente
  /// usado no Brasil.
  ///
  /// [format] padrao eh 'dd/MM/yy HH:mm'
  ///
  /// Para compreender melhor qual os valores que vao
  /// funcionar em [format] verifique os padroes no
  /// link https://pub.dev/documentation/intl/latest/intl/DateFormat-class.html
  String toFormat({format: 'dd/MM/yy HH:mm'}) {
    var formatter = DateFormat(format);

    return formatter.format(this.toDate());
  }
}
