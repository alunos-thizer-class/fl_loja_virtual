import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/view/login/cadastro_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginRecuperarPage extends StatelessWidget {
  static String tag = '/login-recuperar-page';

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var userController = Provider.of<UserController>(context);

    return Scaffold(
      key: _scaffold,
      body: Form(
        key: _form,
        child: Stack(
          children: <Widget>[
            //
            // Imagem de fundo
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/bg-image.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      top: 100, left: 40, right: 40, bottom: 20),
                  child: Image.asset(
                    'assets/images/logo-sem-fundo.png',
                  ),
                ),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Layout.light(),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 15,
                              spreadRadius: 2,
                              color: Layout.dark(.4),
                              offset: Offset(0, 5),
                            )
                          ],
                        ),
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(left: 20, right: 20),
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: 'Email',
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Layout.primary(),
                                  ),
                                ),
                              ),
                              controller: _emailController,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Informe o email';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            Align(
                              alignment: Alignment.centerRight,
                              child: FlatButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text(
                                  'Fazer login',
                                  style:
                                      TextStyle(color: Layout.secondaryDark()),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: FlatButton(
                                onPressed: () async {
                                  if (_form.currentState.validate()) {
                                    String error = await userController
                                        .recuperarSenhaPorEmail(
                                            _emailController.text);

                                    snackBarInfo((error != null)
                                        ? error
                                        : 'Verifique sua nova senha em seu email');
                                  }
                                },
                                color: Layout.primary(),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                child: Text(
                                  'Recuperar Senha',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .copyWith(
                                        color: Layout.light(),
                                        fontSize: 18,
                                      ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30, right: 20),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                            onPressed: () => Navigator.of(context)
                                .pushNamed(CadastroPage.tag),
                            child: Text('Não tem uma conta? Cadastre-se'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      backgroundColor: Layout.secondary(),
    );
  }
}
