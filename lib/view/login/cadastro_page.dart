import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:fl_loja_virtual/view/home/home_page.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/view/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CadastroPage extends StatelessWidget {
  static String tag = '/cadastro-page';

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _senha = TextEditingController();

  final Map<String, dynamic> _data = {};

  @override
  Widget build(BuildContext context) {
    var userController = Provider.of<UserController>(context);

    return Scaffold(
      key: _scaffold,
      body: Form(
        key: _form,
        child: Stack(
          children: <Widget>[
            //
            // Imagem de fundo
            //
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/bg-image.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      top: 100, left: 40, right: 40, bottom: 20),
                  child: Image.asset(
                    'assets/images/logo-sem-fundo.png',
                  ),
                ),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Layout.light(),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 15,
                              spreadRadius: 2,
                              color: Layout.dark(.4),
                              offset: Offset(0, 5),
                            )
                          ],
                        ),
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(left: 20, right: 20),
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: 'Nome',
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Layout.primary(),
                                  ),
                                ),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Campo obrigatório';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _data['nome'] = value;
                              },
                            ),
                            SizedBox(height: 20),
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: 'Email',
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Layout.primary(),
                                  ),
                                ),
                              ),
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Campo obrigatório';
                                } else if (!value.contains('@')) {
                                  return 'Preencha com um email válido';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _data['email'] = value;
                              },
                            ),
                            SizedBox(height: 20),
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: 'Senha',
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Layout.primary(),
                                  ),
                                ),
                              ),
                              controller: _senha,
                              obscureText: true,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Campo obrigatório';
                                } else if (value.length < 6) {
                                  return 'Pelo menos 6 caracteres';
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _data['senha'] = value;
                              },
                            ),
                            SizedBox(height: 20),
                            TextFormField(
                              decoration: InputDecoration(
                                hintText: 'Confirmar Senha',
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Layout.primary(),
                                  ),
                                ),
                              ),
                              obscureText: true,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Campo obrigatório';
                                } else if (value != _senha.text) {
                                  return 'As senhas não são iguais';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 30),
                            SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: FlatButton(
                                onPressed: () async {
                                  if (_form.currentState.validate()) {
                                    // Ao salvar os dados do formulario ficarao
                                    // retidos em _data automaticamente
                                    _form.currentState.save();

                                    String error = await userController
                                        .criarContaPorEmailSenha(
                                      nome: _data['nome'],
                                      email: _data['email'],
                                      senha: _data['senha'],
                                    );

                                    if (error != null) {
                                      snackBarDanger(error);
                                      return;
                                    }

                                    // Se ate aqui nao deu nenhum erro,
                                    // eh porque deu tudo certo e o login já esta
                                    // salvo no UserController.
                                    // Redireciona para pagina inicial
                                    Navigator.of(context)
                                        .popUntil((route) => route.isFirst);
                                    Navigator.of(context)
                                        .pushReplacementNamed(HomePage.tag);
                                  }
                                },
                                color: Layout.primary(),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                child: Text(
                                  'Criar conta',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .copyWith(
                                        color: Layout.light(),
                                        fontSize: 18,
                                      ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 0, right: 20),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                            onPressed: () => Navigator.of(context)
                                .popAndPushNamed(LoginPage.tag),
                            child: Text('Fazer login'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      backgroundColor: Layout.secondary(),
    );
  }
}
