import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/view/home/home_page.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/view/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatelessWidget {
  static String tag = '/splash-page';

  @override
  Widget build(BuildContext context) {
    // Recupera a instancia do controller
    var userController = Provider.of<UserController>(context);

    userController.checkIsLoggedIn().then((UserAuthStatus status) {
      // Mata todas as rotas anteriores
      Navigator.of(context).popUntil((route) => route.isFirst);

      if (status == UserAuthStatus.loggedIn) {
        Navigator.of(context).popAndPushNamed(HomePage.tag);
      } else {
        Navigator.of(context).popAndPushNamed(LoginPage.tag);
      }
    });

    return Scaffold(
      backgroundColor: Layout.secondary(),
      body: Stack(
        children: <Widget>[
          //
          // Imagem de fundo
          //
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/bg-image.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
