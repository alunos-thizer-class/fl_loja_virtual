import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/model/compra_model.dart';
import 'package:fl_loja_virtual/view/compras/compra_detalhe_page.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:provider/provider.dart';

class ComprasPage extends StatelessWidget {
  static String tag = '/compras-page';

  @override
  Widget build(BuildContext context) {
    var userController = Provider.of<UserController>(context);

    var queryCompras = Firestore.instance
        .collection('compra')
        .where('uid', isEqualTo: userController.user.uid)
        .orderBy('data', descending: true)
        .snapshots();

    var content = StreamBuilder(
      stream: queryCompras,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        }

        if (snapshot.data.documents.length == 0) {
          return Center(child: Text('Nenhuma compra'));
        }

        return ListView.builder(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          itemCount: snapshot.data.documents.length,
          itemBuilder: (BuildContext context, int i) {
            var docSnp = snapshot.data.documents[i];
            var item = CompraModel.fromJson(docSnp.reference, docSnp.data);

            return Container(
              margin: EdgeInsets.fromLTRB(20, (i == 0 ? 10 : 0), 20, 10),
              decoration: BoxDecoration(
                color: Layout.light(),
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListTile(
                isThreeLine: true,
                title: Text('#${item.sequence} - ${item.valorTotal.toBRL()}'),
                subtitle: Text('${item.data.toFormat()} \n${item.status}'),
                trailing: IconButton(
                  icon: FaIcon(FontAwesomeIcons.clipboardList),
                  color: Layout.primary(),
                  onPressed: () => null,
                ),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CompraDetalhePage(item.docRef),
                    ),
                  );
                },
              ),
            );
          },
        );
      },
    );

    return Layout.render(
      context,
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20, bottom: 0),
            child: Text(
              'Minhas Compras',
              style: Theme.of(context).textTheme.headline6.copyWith(
                    color: Layout.light(),
                  ),
            ),
          ),
          Expanded(child: content),
        ],
      ),
      bottomItemSelected: 1,
    );
  }
}
