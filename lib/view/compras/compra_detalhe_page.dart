import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fl_loja_virtual/constants.dart';
import 'package:fl_loja_virtual/controller/compra_detalhe_controller.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'dart:io';

class CompraDetalhePage extends StatefulWidget {
  static String tag = '/compra-detalhe-page';

  // Id da compra
  final DocumentReference compraRef;

  CompraDetalhePage(this.compraRef);

  @override
  _CompraDetalhePageState createState() => _CompraDetalhePageState();
}

class _CompraDetalhePageState extends State<CompraDetalhePage> {
  bool _loading = false;

  final _controller = CompraDetalheController();

  @override
  Widget build(BuildContext context) {
    return Layout.render(
      context,
      FutureBuilder(
        future: _controller.loadCompraComItens(widget.compraRef),
        builder: (
          BuildContext context,
          AsyncSnapshot<CompraDetalheController> snapshot,
        ) {
          if (snapshot.data == null || _controller.compra == null) {
            return Center(child: CircularProgressIndicator());
          }

          var compra = _controller.compra;
          var items = _controller.items;

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 20, bottom: 0),
                child: Text(
                  'Compra #${compra.sequence}',
                  style: Theme.of(context).textTheme.headline6.copyWith(
                        color: Layout.light(),
                      ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Layout.light(),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  margin: const EdgeInsets.all(20),
                  padding: const EdgeInsets.all(20),
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Realizada em ${compra.data.toFormat()}'),
                      SizedBox(height: 20),
                      Row(
                        children: <Widget>[
                          Text('Status: '),
                          Text(
                            compra.status,
                            style: TextStyle(color: Colors.blue[900]),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Text('Total em Itens: ${compra.valorItens.toBRL()}'),
                      Text(
                          'Frete por ${compra.tipoFrete}: ${compra.valorFrete.toBRL()} - ${compra.prazoFrete} dia(s)'),
                      SizedBox(height: 10),
                      Text('Total: ${compra.valorTotal.toBRL()}'),
                      SizedBox(height: 20),
                      (compra.status == statusAguardandoPagamento)
                          ? _renderBotaoPagar()
                          : SizedBox(),
                      Text('Itens:', style: TextStyle(fontSize: 18)),
                      Expanded(
                        child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: items.length,
                          itemBuilder: (BuildContext context, int i) {
                            var item = items[i];

                            var descProduto = '${item.quantidade}';
                            descProduto += ' X ${item.valorUnitario.toBRL()}';
                            descProduto += '\n${item.cor}';

                            return Container(
                              margin: const EdgeInsets.symmetric(vertical: 5),
                              decoration: BoxDecoration(
                                color: Layout.dark(.1),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: ListTile(
                                dense: true,
                                isThreeLine: true,
                                leading: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.network(item.fkProduto.imagem),
                                ),
                                title: Text('${item.titulo}'),
                                subtitle: Text(descProduto),
                                trailing: Text('${item.valorTotal.toBRL()}'),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
      bottomItemSelected: 1,
    );
  }

  Widget _renderBotaoPagar() {
    var botao;

    // Mostra barra enquanto carrega
    if (_loading) {
      return Center(
          child: LinearProgressIndicator(
        backgroundColor: Colors.blue[700],
      ));
    }

    if (_controller.compra.tipoPagamento == TipoPagto.deposito.value) {
      botao = RaisedButton(
        child: Text('Enviar Comprovante'),
        color: Layout.primary(),
        textColor: Layout.light(),
        onPressed: _enviarComprovante,
      );
    }

    if (_controller.compra.tipoPagamento == TipoPagto.boleto.value) {
      botao = RaisedButton(
        child: Text('Gerar boleto'),
        color: Layout.primary(),
        textColor: Layout.light(),
        onPressed: () => print('[TODO] Implementar geração de boleto'),
      );
    }

    if (_controller.compra.tipoPagamento == TipoPagto.cartao.value) {
      botao = RaisedButton(
        child: Text('Pagar com cartão'),
        color: Layout.primary(),
        textColor: Layout.light(),
        onPressed: () => print('[TODO] Implementar pagamento por Cartão'),
      );
    }

    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(bottom: 20),
      child: botao,
    );
  }

  _enviarComprovante() async {
    //
    // Abre para selecionar arquivo
    File file = await FilePicker.getFile(type: FileType.image);

    // Valida se foi selecionado algum
    if (file == null) {
      return;
    }

    setState(() {
      _loading = true;
    });

    // Verifica qual a extenssao do arquivo
    String ext = file.path.replaceAll(RegExp(r'.+\.'), '');

    // Define local onde sera gravado la no Firebase e novo
    // nome do arquivo
    String filename = 'comprovantes/';
    filename += '${DateTime.now().millisecondsSinceEpoch}-';
    filename += 'compra-${_controller.compra.sequence}';
    filename += '.$ext';

    // Define esta referencia la no Firebase
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(filename);

    // Inclui arquivo para ser enviado
    StorageUploadTask uploadTask = storageReference.putFile(file);

    // Aguarda ate upload terminar
    await uploadTask.onComplete;

    // Recupera a URL do arquivo enviado
    var fileUrl = await storageReference.getDownloadURL();

    // Atualiza campos da model e salva no banco
    _controller.compra.comprovante = fileUrl;
    _controller.compra.status = statusVerificacaoPagamento;
    await _controller.compra.update();

    // Dispara mensagem de sucesso
    snackBarSuccess('Comprovante de pagamento enviado com sucesso!');

    // Atualiza pagina
    setState(() {
      _loading = false;
    });
  }
}
