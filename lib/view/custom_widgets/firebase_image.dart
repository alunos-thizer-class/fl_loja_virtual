import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class FirebaseImage extends StatelessWidget {
  final String imagePath;
  final Image Function(BuildContext, String) builder;

  /// Esta classe ira pegar o caminho da imagem
  /// que deve estar no Firebase Storage vinculado
  /// ao projeto e retornar no [builder] a URL
  /// para esta imagem.
  ///
  /// O [builder] deve obrigatoriamente ser uma
  /// funcao que retornara o <b>Image.network(imageUrl)</b>
  ///
  /// Exemplo de uso:
  ///
  /// ```dart
  /// FirebaseImage(
  ///   'produtos/minha-imagem-de-produto.jpg',
  ///   builder: (String imageUrl) {
  ///     return Image.network(
  ///       imageUrl,
  ///       fit: BoxFit.cover,
  ///     );
  ///   },
  /// );
  /// ```
  ///
  /// Depende do package https://pub.dev/packages/firebase_storage
  ///
  FirebaseImage(
    this.imagePath, {
    Key key,
    @required this.builder,
  })  : assert(builder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: FirebaseStorage.instance.ref().child(imagePath).getDownloadURL(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data != null) {
          return this.builder(context, snapshot.data);
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
