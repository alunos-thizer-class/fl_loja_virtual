import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_item_store.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_store.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/model/favorito_model.dart';
import 'package:fl_loja_virtual/model/produto_model.dart';
import 'package:fl_loja_virtual/view/carrinho/carrinho_page.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:provider/provider.dart';

class ProdutoPage extends StatefulWidget {
  static String tag = '/produto-page';

  ProdutoPage(this.produtoRef);

  final DocumentReference produtoRef;

  @override
  _ProdutoPageState createState() => _ProdutoPageState();
}

class _ProdutoPageState extends State<ProdutoPage> {
  //
  List<String> imagens = [];
  List<String> cores = [];
  int corSelecionada = 0;
  int currentPic = 0;

  // Quando este objeto nao estiver
  // mais nulo entao nao serao feitas
  // novas consultas no Firestore pelo
  // produto, suas imagens e cores
  ProdutoModel produto;

  @override
  Widget build(BuildContext context) {
    var sController = ScrollController();
    var listViewItemWidth = MediaQuery.of(context).size.width - 40;

    var userController = Provider.of<UserController>(context);

    var carrinho = Provider.of<CarrinhoStore>(context);

    var content = FutureBuilder(
      future: buscaDetalhesProduto(),
      builder: (context, AsyncSnapshot<ProdutoModel> snapshot) {
        if (snapshot.data == null) {
          return Center(child: CircularProgressIndicator());
        }

        produto = snapshot.data;

        return Container(
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
          ),
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 4,
                child: Stack(
                  children: [
                    ListView.builder(
                      controller: sController,
                      scrollDirection: Axis.horizontal,
                      physics: PageScrollPhysics(),
                      itemCount: imagens.length,
                      itemBuilder: (BuildContext context, int i) {
                        return Container(
                          width: listViewItemWidth,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              topRight: Radius.circular(25),
                            ),
                            child: Image.network(
                              imagens[i],
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                    ),
                    Container(
                      width: listViewItemWidth,
                      padding: const EdgeInsets.only(top: 10),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: StreamBuilder(
                          stream: Firestore.instance
                              .collection('favorito')
                              // .where('excluido', isEqualTo: false)
                              .where('fk_produto', isEqualTo: produto.docRef)
                              .where('uid', isEqualTo: userController.user.uid)
                              .snapshots(),
                          builder: (
                            context,
                            AsyncSnapshot<QuerySnapshot> snapshot,
                          ) {
                            if (snapshot.data == null) {
                              return SizedBox.shrink();
                            }

                            FavoritoModel favorito;
                            if (snapshot.data.documents.isNotEmpty) {
                              favorito = FavoritoModel.fromDocument(
                                snapshot.data.documents.first,
                              );
                            }

                            return FlatButton(
                              child: FaIcon(
                                (favorito != null && favorito.excluido == false)
                                    ? FontAwesomeIcons.solidHeart
                                    : FontAwesomeIcons.heart,
                                color: Layout.light(),
                              ),
                              color: Colors.red[300],
                              shape: CircleBorder(),
                              onPressed: () => toggleFavorito(
                                favorito,
                                produto,
                                userController.user.uid,
                              ),
                              padding: EdgeInsets.all(10),
                            );
                          },
                        ),
                      ),
                    ),
                    Container(
                      width: listViewItemWidth,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.chevronLeft,
                            color: Layout.light(),
                          ),
                          onPressed: () {
                            sController.animateTo(
                              (currentPic - 1) * listViewItemWidth,
                              duration: Duration(milliseconds: 700),
                              curve: Curves.ease,
                            );

                            currentPic--;
                            if (currentPic < 0) {
                              currentPic = 0;
                            }
                          },
                        ),
                      ),
                    ),
                    Container(
                      width: listViewItemWidth,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.chevronRight,
                            color: Layout.light(),
                          ),
                          onPressed: () {
                            sController.animateTo(
                              (currentPic + 1) * listViewItemWidth,
                              duration: Duration(milliseconds: 700),
                              curve: Curves.ease,
                            );

                            currentPic++;
                            if (currentPic > (imagens.length - 1)) {
                              currentPic = imagens.length - 1;
                            }
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  color: Layout.light(),
                  height: 100,
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width:
                                (MediaQuery.of(context).size.width - 60) * .65,
                            child: Text(
                              produto.titulo,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              produto.preco.toBRL(),
                              textAlign: TextAlign.end,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                    color: Layout.primary(),
                                  ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: List<Widget>.generate(
                          cores.length,
                          (iCor) {
                            return Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: GestureDetector(
                                child: Chip(
                                  label: Text(
                                    cores[iCor],
                                    style: TextStyle(
                                      color: (corSelecionada == iCor)
                                          ? Colors.white
                                          : Colors.black,
                                    ),
                                  ),
                                  backgroundColor: (corSelecionada == iCor)
                                      ? Layout.primaryDark(.6)
                                      : Colors.grey[300],
                                ),
                                onTap: () {
                                  setState(() {
                                    corSelecionada = iCor;
                                  });
                                },
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: 20),
                      Expanded(
                        child: ListView(
                          children: <Widget>[
                            Text(
                              'Detalhes',
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            SizedBox(height: 10),
                            Text(produto.detalhe),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                child: Container(
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Layout.primary(),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'COMPRAR',
                      style: Theme.of(context).textTheme.headline6.copyWith(
                            color: Colors.white,
                          ),
                    ),
                  ),
                ),
                onTap: () {
                  carrinho.addItem(CarrinhoItemStore(
                    produto: produto,
                    cor: cores.isNotEmpty ? cores[corSelecionada] : "Cor única",
                  ));

                  Navigator.of(context).pushNamed(CarrinhoPage.tag);
                },
              ),
            ],
          ),
        );
      },
    );

    return Layout.render(context, content);
  }

  Future<ProdutoModel> buscaDetalhesProduto() async {
    //
    // Isso impede consultas repetidas ao usar
    // o setState para marcar a cor selecionada
    if (produto != null) {
      return produto;
    }

    var docSnp = await widget.produtoRef.get();
    var result = ProdutoModel.fromDocument(docSnp);

    // Adiciona imagem principal
    imagens = [];
    imagens.add(result.imagem);

    var fotos = await Firestore.instance
        .collection('foto')
        .where('fk_produto', isEqualTo: widget.produtoRef)
        .where('excluido', isEqualTo: false)
        .getDocuments();

    for (var foto in fotos.documents) {
      imagens.add(foto.data['url']);
    }

    var coresDoc = await Firestore.instance
        .collection('cor')
        .where('fk_produto', isEqualTo: widget.produtoRef)
        .where('excluido', isEqualTo: false)
        .getDocuments();

    cores = [];
    for (var cor in coresDoc.documents) {
      cores.add(cor.data['texto']);
    }

    return result;
  }

  toggleFavorito(FavoritoModel favorito, ProdutoModel item, String uid) {
    // Update ou Insert na model gera uma modificacao
    // no banco e isso eh um gatilho no Stream.
    // Então o efeito sera automatico caso esteja
    // usando StreamBuilder

    if (favorito == null) {
      FavoritoModel(
        fkProduto: item,
        uid: uid,
      ).insert();
    } else {
      favorito.excluido = !favorito.excluido;
      favorito.update();
    }
  }
}
