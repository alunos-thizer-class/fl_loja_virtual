import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/model/categoria_model.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/controller/categoria/lista_produto_store.dart';
import 'package:fl_loja_virtual/view/produto/produto_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:fl_loja_virtual/functions.dart';
import 'package:provider/provider.dart';

class CategoriaPage extends StatefulWidget {
  static String tag = '/categoria-page';

  final CategoriaModel categoria;

  CategoriaPage(this.categoria);

  @override
  _CategoriaPageState createState() => _CategoriaPageState();
}

class _CategoriaPageState extends State<CategoriaPage> {
  ListaProduto listaProduto;

  @override
  Widget build(BuildContext context) {
    //
    // carrega a store produto apenas uma vez
    if (listaProduto == null) {
      var userController = Provider.of<UserController>(context);

      listaProduto = ListaProduto(
        widget.categoria,
        userController.user.uid,
      );
    }

    var content = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: TextFormField(
            decoration: InputDecoration(
              suffixIcon: Icon(Icons.search, color: Layout.primary()),
              hintText: 'Pesquisa',
              contentPadding: const EdgeInsets.only(left: 20),
              isDense: true,
              fillColor: Layout.light(.6),
              filled: true,
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Layout.primary(.3),
                ),
                borderRadius: BorderRadius.circular(20),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Layout.primary(.3),
                ),
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            onChanged: listaProduto.pesquisa,
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          height: 50,
          child: Observer(
            builder: (context) {
              return ListView(
                scrollDirection: Axis.horizontal,
                children: List.generate(
                  ListaProdutoOrder.values.length,
                  (iOrder) {
                    var prodOrderValue = ListaProdutoOrder.values[iOrder];
                    var nomeOrderItem = 'Ordenar';

                    switch (prodOrderValue) {
                      case ListaProdutoOrder.aParaZ:
                        nomeOrderItem = 'de A-Z';
                        break;
                      case ListaProdutoOrder.zParaA:
                        nomeOrderItem = 'de Z-A';
                        break;
                      case ListaProdutoOrder.maiorValor:
                        nomeOrderItem = 'Maior valor';
                        break;
                      case ListaProdutoOrder.menorValor:
                        nomeOrderItem = 'Menor Valor';
                        break;
                      default:
                    }

                    return Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 1,
                        vertical: 0,
                      ),
                      child: GestureDetector(
                        child: Chip(
                          label: Text(
                            nomeOrderItem,
                            style: TextStyle(color: Layout.light()),
                          ),
                          backgroundColor: Layout.dark(
                            (listaProduto.ordenacao == prodOrderValue)
                                ? 0.6
                                : 1,
                          ),
                        ),
                        onTap: () => listaProduto.reordenar(prodOrderValue),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Categoria: ${widget.categoria.nome}',
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Layout.light(),
                    ),
              ),
              Observer(
                builder: (context) {
                  return FlatButton(
                    child: FaIcon(
                      listaProduto.favoritos
                          ? FontAwesomeIcons.solidHeart
                          : FontAwesomeIcons.heart,
                      color: Layout.light(),
                      size: 16,
                    ),
                    color: Colors.red[300],
                    shape: CircleBorder(),
                    onPressed: () => listaProduto.toggleFavoritos(),
                    padding: EdgeInsets.all(10),
                  );
                },
              )
            ],
          ),
        ),
        Expanded(
          child: Observer(
            builder: (context) {
              if (listaProduto.loading) {
                return Center(child: CircularProgressIndicator());
              }

              if (listaProduto.lista.isEmpty) {
                return Center(
                  child: Text('Nenhum produto encontrado aqui'),
                );
              }

              return ListView.builder(
                itemCount: listaProduto.lista.length,
                itemBuilder: (BuildContext context, int i) {
                  var produto = listaProduto.lista[i];

                  return GestureDetector(
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                      decoration: BoxDecoration(
                        color: Layout.light(),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(2, 3),
                            color: Layout.dark(.1),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 70,
                        child: Row(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                              ),
                              child: Image.network(produto.imagem),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Text(produto.titulo),
                                  Text(
                                    produto.preco.toBRL(),
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Layout.primaryLight(),
                                    ),
                                  ),
                                  Text(
                                    produto.chamada,
                                    style: TextStyle(color: Layout.dark(.6)),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 10),
                            FaIcon(
                              FontAwesomeIcons.angleDoubleRight,
                              color: Layout.primaryLight(),
                            ),
                            SizedBox(width: 10),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ProdutoPage(produto.docRef),
                        ),
                      );
                    },
                  );
                },
              );
            },
          ),
        ),
      ],
    );

    return Layout.render(
      context,
      content,
    );
  }
}
