import 'package:fl_loja_virtual/controller/carrinho/carrinho_store.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/view/carrinho/carrinho_page.dart';
import 'package:fl_loja_virtual/view/compras/compras_page.dart';
import 'package:fl_loja_virtual/view/favoritos/favoritos_page.dart';
import 'package:fl_loja_virtual/view/home/home_page.dart';
import 'package:fl_loja_virtual/view/login/login_page.dart';
import 'package:fl_loja_virtual/view/perfil/perfil_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class Layout {
  static Widget render(
    BuildContext context,
    Widget child, {
    Widget floatingActionButton,
    int bottomItemSelected,
  }) {
    var userController = Provider.of<UserController>(context);
    var carrinho = Provider.of<CarrinhoStore>(context);

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/bg-image.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Column(
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(30, 20, 10, 20),
                        child: GestureDetector(
                          child: FaIcon(
                            FontAwesomeIcons.userCog,
                            color: Layout.light(),
                            size: 24,
                          ),
                          onTap: () =>
                              Navigator.of(context).pushNamed(PerfilPage.tag),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          child: Text(
                            userController.user.displayName,
                            style:
                                Theme.of(context).textTheme.subtitle1.copyWith(
                                      color: Layout.light(),
                                      fontSize: 18,
                                      fontStyle: FontStyle.italic,
                                    ),
                          ),
                          onTap: () =>
                              Navigator.of(context).pushNamed(PerfilPage.tag),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 30),
                        child: GestureDetector(
                          onTap: () =>
                              Navigator.of(context).pushNamed(CarrinhoPage.tag),
                          child: Row(
                            children: <Widget>[
                              FaIcon(
                                FontAwesomeIcons.shoppingBag,
                                color: Layout.primaryLight(),
                                size: 24,
                              ),
                              (carrinho.items.isNotEmpty)
                                  ? ConstrainedBox(
                                      constraints: BoxConstraints(
                                        minWidth: 30,
                                      ),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Layout.light(),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        margin: const EdgeInsets.only(left: 5),
                                        padding: const EdgeInsets.all(5),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Observer(
                                            builder: (context) {
                                              return Text(
                                                carrinho.items.length
                                                    .toString(),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText2
                                                    .copyWith(
                                                      color:
                                                          Layout.primaryLight(),
                                                    ),
                                                textAlign: TextAlign.center,
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(child: child),
              ],
            ),
            // Expanded(child: child),
          ],
        ),
      ),
      floatingActionButton: floatingActionButton,
      backgroundColor: Layout.secondary(),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text('Início'),
            icon: FaIcon(
              FontAwesomeIcons.umbrellaBeach,
              size: 30,
            ),
          ),
          BottomNavigationBarItem(
            title: Text('Compras'),
            icon: FaIcon(
              FontAwesomeIcons.solidStar,
              size: 30,
            ),
          ),
          BottomNavigationBarItem(
            title: Text('Favoritos'),
            icon: FaIcon(
              FontAwesomeIcons.solidHeart,
              size: 30,
            ),
          ),
          BottomNavigationBarItem(
            title: Text('Sair'),
            icon: FaIcon(
              FontAwesomeIcons.signOutAlt,
              size: 30,
            ),
          ),
        ],
        currentIndex: bottomItemSelected ?? 0,
        selectedItemColor: (bottomItemSelected == null)
            ? Layout.dark(.3)
            : Layout.primaryLight(),
        unselectedItemColor: Layout.dark(.3),
        backgroundColor: Layout.light(),
        type: BottomNavigationBarType.fixed,
        onTap: (int i) {
          switch (i) {
            case 0: // Inicio
              Navigator.of(context).pushNamed(HomePage.tag);
              break;
            case 1: // Compras
              Navigator.of(context).pushNamed(ComprasPage.tag);
              break;
            case 2: // Favoritos
              Navigator.of(context).pushNamed(FavoritosPage.tag);
              break;
            case 3: // Sair

              // Desloga usuario
              userController.signOut();

              // Navega para pagina de login
              Navigator.of(context).popUntil((route) => route.isFirst);
              Navigator.of(context).popAndPushNamed(LoginPage.tag);
              break;
          }
        },
      ),
    );
  }

  // Color(0xffE0CF9D);
  // Color(0xffF1F0EC);
  // Color(0xffCAAF74);
  // Color(0xffB88A52);
  // Color(0xff2C3527);
  // Color(0xff9C6D3E);
  // Color(0xffFDAC25);

  static Color primary([double opacity = 1]) =>
      Color(0xff195738).withOpacity(opacity);
  static Color primaryLight([double opacity = 1]) =>
      Color(0xff007d40).withOpacity(opacity);
  static Color primaryDark([double opacity = 1]) =>
      Color(0xff123D27).withOpacity(opacity);

  static Color secondary([double opacity = 1]) =>
      Color(0xffddc199).withOpacity(opacity);
  static Color secondaryLight([double opacity = 1]) =>
      Color(0xffE0CF9D).withOpacity(opacity);
  static Color secondaryDark([double opacity = 1]) =>
      Color(0xffce9150).withOpacity(opacity);
  static Color secondaryHight([double opacity = 1]) =>
      Color(0xffFDAC25).withOpacity(opacity);

  static Color light([double opacity = 1]) =>
      Color(0xfff0ece1).withOpacity(opacity);
  static Color dark([double opacity = 1]) =>
      Color(0xff333333).withOpacity(opacity);
}
