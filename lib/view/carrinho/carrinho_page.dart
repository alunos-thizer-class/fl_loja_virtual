import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_store.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_frete_store.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/constants.dart';
import 'package:fl_loja_virtual/model/endereco_model.dart';
import 'package:fl_loja_virtual/view/carrinho/finaliza_page.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/view/perfil/perfil_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:sigepweb/sigepweb.dart';

class CarrinhoPage extends StatefulWidget {
  static String tag = '/carrinho';

  @override
  _CarrinhoPageState createState() => _CarrinhoPageState();
}

class _CarrinhoPageState extends State<CarrinhoPage> {
  CarrinhoStore _carrinho;
  UserController _userController;

  @override
  Widget build(BuildContext context) {
    _carrinho = Provider.of<CarrinhoStore>(context);
    _userController = Provider.of<UserController>(context);

    var content = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 10),
          child: Text(
            'Carrinho',
            style: Theme.of(context).textTheme.headline6.copyWith(
                  color: Layout.light(),
                ),
          ),
        ),
        Observer(
          builder: (context) {
            return Expanded(
              child: _carrinho.items.isNotEmpty
                  ? _getListaProdutos()
                  : Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('Nenhum produto ainda'),
                          RaisedButton(
                            onPressed: () => Navigator.of(context).popUntil(
                              (route) => route.isFirst,
                            ),
                            color: Colors.blue[900],
                            textColor: Layout.light(),
                            child: Text('Ver produtos da loja'),
                          )
                        ],
                      ),
                    ),
            );
          },
        ),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Layout.light(),
                Layout.light(.6),
              ],
            ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(70),
              // topRight: Radius.circular(25),
            ),
          ),
          margin: const EdgeInsets.only(top: 10, left: 10),
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Observer(
                builder: (context) {
                  return Container(
                    width: 100,
                    height: 120,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        InkWell(
                          onTap: () => selecionaFrete(context),
                          borderRadius: BorderRadius.circular(50),
                          child: Container(
                            width: 90,
                            height: 90,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Layout.dark(.1),
                            ),
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.truck,
                                  color: Layout.primaryDark(),
                                ),
                                (_carrinho.frete != null)
                                    ? Text(
                                        _carrinho.frete.nome,
                                        textAlign: TextAlign.center,
                                      )
                                    : Text(
                                        'Selecione!',
                                        style: TextStyle(color: Colors.red),
                                        textAlign: TextAlign.center,
                                      ),
                              ],
                            ),
                          ),
                        ),
                        (_carrinho.frete != null)
                            ? Text(
                                'Prazo: ${_carrinho.frete.prazo} dia(s)',
                                textAlign: TextAlign.center,
                              )
                            : SizedBox(),
                      ],
                    ),
                  );
                },
              ),
              SizedBox(width: 40),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Observer(
                                  builder: (context) => Text(
                                    '${_carrinho.items.length} Produtos:',
                                  ),
                                ),
                                Text('Frete:'),
                                SizedBox(height: 10),
                                Text(
                                  'Total:',
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          child: Observer(
                            builder: (context) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(_carrinho.valorItems.toBRL()),
                                  Text(_carrinho.valorFrete.toBRL()),
                                  SizedBox(height: 10),
                                  Text(
                                    _carrinho.valorTotal.toBRL(),
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    Divider(),
                    SizedBox(
                      width: double.infinity,
                      child: Observer(
                        builder: (context) {
                          return RaisedButton(
                            child: Text('Continuar'),
                            color: Layout.primary(),
                            disabledColor: Layout.primary(.3),
                            textColor: Layout.light(),
                            onPressed: (_carrinho.frete != null)
                                ? () => Navigator.of(context)
                                    .pushNamed(FinalizaPage.tag)
                                : null,
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          color: Layout.dark(.3),
          height: 1,
        ),
      ],
    );

    return Layout.render(context, content);
  }

  _getListaProdutos() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _carrinho.items.length,
      itemBuilder: (BuildContext context, int i) {
        var item = _carrinho.items[i];

        return Card(
          margin: (i == 0)
              ? const EdgeInsets.fromLTRB(20, 0, 20, 0)
              : const EdgeInsets.fromLTRB(20, 10, 20, 0),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.network(
                        item.produto.imagem,
                        width: 50,
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(item.produto.titulo),
                            Text(
                              item.produto.chamada,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .copyWith(
                                    color: Layout.dark(.6),
                                  ),
                              overflow: TextOverflow.fade,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      Material(
                        color: Colors.red[300],
                        borderRadius: BorderRadius.circular(25),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(25),
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Icon(
                              FontAwesomeIcons.solidTrashAlt,
                              size: 16,
                              color: Layout.light(),
                            ),
                          ),
                          onTap: () => _carrinho.remove(item),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(FontAwesomeIcons.caretLeft),
                      onPressed: () => item.decrement(),
                    ),
                    Observer(
                      builder: (context) => Text(item.quantidade.toString()),
                    ),
                    IconButton(
                      icon: Icon(FontAwesomeIcons.caretRight),
                      onPressed: () => item.increment(),
                    ),
                    Observer(
                      builder: (context) => Text(item.valorItem.toBRL()),
                    ),
                    Expanded(child: SizedBox()),
                    Chip(
                      label: Text(item.cor),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void selecionaFrete(BuildContext context) {
    //
    // Se nao ha produtos no carrinho nem continua
    if (_carrinho.items.isEmpty) {
      return;
    }

    showDialog(
      context: context,
      builder: (context) {
        return FutureBuilder<List<Map<String, dynamic>>>(
          future: _loadFretesValores(context),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Center(child: CircularProgressIndicator());
            }

            return AlertDialog(
              contentPadding: const EdgeInsets.symmetric(horizontal: 10),
              title: Text('Selecione o frete'),
              content: Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      var item = snapshot.data[index];

                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey[100],
                        ),
                        margin: const EdgeInsets.all(5),
                        child: ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Layout.secondaryDark(),
                            foregroundColor: Layout.light(),
                            child: Text(item['nome'][0]),
                          ),
                          title: Text(item['nome']),
                          subtitle: Text((item['valor'] as double).toBRL()),
                          trailing: Text('${item['prazo']} dia(s)'),
                          onTap: () {
                            _carrinho.frete = CarrinhoFreteStore(
                              nome: item['nome'],
                              valor: item['valor'],
                              prazo: item['prazo'],
                            );

                            Navigator.of(context).pop();
                          },
                        ),
                      );
                    },
                  ),
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  /// Retorna uma lista com os valores do frete
  /// para esta compra. O calculo é feito através de uma API dos
  /// correios chamada Sigepweb
  Future<List<Map<String, dynamic>>> _loadFretesValores(
    BuildContext context,
  ) async {
    // Busca endereco do cadastro do usuario
    var endereco = await Firestore.instance
        .collection('endereco')
        .where('uid', isEqualTo: _userController.user.uid)
        .getDocuments();

    // Cria model se for possível
    var enderecoModel = endereco.documents.isNotEmpty
        ? EnderecoModel.fromDocument(endereco.documents.first)
        : null;

    // Usuario ainda nao completou seu endereco entao
    // ele eh obrigado a fazer isso antes de continuar
    if (endereco.documents.isEmpty || enderecoModel.cep.isEmpty) {
      Navigator.of(context).pop(); // Fecha modal antes
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) {
          return PerfilPage(
            snackbarMessage:
                'Obrigatório completar seu endereço para continuar',
          );
        },
      ));
    }

    List<Map<String, dynamic>> items = [];

    // Verifica se o cep do usuario esta no range da loja.
    // Essa logica pode nao refletir de fato a realidade, serve ate
    // aqui apenas para lembrar que eu morando no PR nao posso receber
    // uma encomenda de SP por motoboy...
    // O certo aqui eh ter uma logica que de fato reflita a realidade
    // junto aos correios
    if (enderecoModel.cep.substring(0, 2) == cepLoja.substring(0, 2)) {
      items.add({
        'nome': 'Motoboy',
        'valor': valorMotoboy,
        'prazo': 1,
      });
      //
    } else {
      print(
          'Motoboy nao apareceu porque o cep do usuario eh ${enderecoModel.cep} e da loja ${cepLoja}');
    }

    var sigep = Sigepweb(contrato: SigepContrato.semContrato());

    // Note que aqui fixamos um peso, o que deve ser evitado caso este
    // projeto seja de fato colocado em producao.
    // O mesmo deve ser reavaliado com relação ao tamanho da caixa
    // da encomenda, os valores podem não bater.
    var servicos = await sigep.calcPrecoPrazo(
      cepOrigem: cepLoja,
      cepDestino: enderecoModel.cep,
      // valorPeso: 1,
    );

    for (var servico in servicos) {
      items.add({
        'nome': servico.nome,
        'valor': servico.valor,
        'prazo': servico.prazoEntrega,
      });
    }

    // Se for preciso, adicione aqui novas modalidades de frete
    // como transportadora, por exemplo

    return items;
  }
}
