import 'package:fl_loja_virtual/constants.dart';
import 'package:fl_loja_virtual/controller/carrinho/carrinho_store.dart';
import 'package:fl_loja_virtual/controller/finaliza_compra_controller.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:fl_loja_virtual/view/compras/compra_detalhe_page.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class FinalizaPage extends StatefulWidget {
  static String tag = '/carrinho-finaliza';

  @override
  _FinalizaPageState createState() => _FinalizaPageState();
}

class _FinalizaPageState extends State<FinalizaPage> {
  var _loading = false;
  TipoPagto _tipoPagto;

  @override
  Widget build(BuildContext context) {
    var userController = Provider.of<UserController>(context);
    var carrinho = Provider.of<CarrinhoStore>(context);

    var finalizaController = FinalizaCompraController(
      carrinho: carrinho,
      uid: userController.user.uid,
    );

    var content = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 10),
          child: Text(
            'Método de Pagamento',
            style: Theme.of(context).textTheme.headline6.copyWith(
                  color: Layout.light(),
                ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Card(
                color: Layout.light(),
                child: ListTile(
                  leading: CircleAvatar(
                    child: Icon(FontAwesomeIcons.piggyBank),
                  ),
                  title: Text('Depósito bancário'),
                  subtitle: Text('Libera em até 48 horas'),
                  trailing: Radio(
                    value: TipoPagto.deposito,
                    groupValue: _tipoPagto,
                    activeColor: Colors.blue[900],
                    onChanged: (newVal) => null,
                  ),
                  onTap: () {
                    if (_loading) {
                      return;
                    }
                    setState(() => _tipoPagto = TipoPagto.deposito);
                  },
                ),
              ),
              Card(
                color: Layout.light(),
                child: ListTile(
                  leading: CircleAvatar(
                    child: Icon(FontAwesomeIcons.ticketAlt),
                  ),
                  title: Text('Boleto bancário'),
                  subtitle: Text('Libera em até 48 horas'),
                  trailing: Radio(
                    value: TipoPagto.boleto,
                    groupValue: _tipoPagto,
                    activeColor: Colors.blue[900],
                    onChanged: (newVal) => null,
                  ),
                  onTap: () {
                    if (_loading) {
                      return;
                    }
                    setState(() => _tipoPagto = TipoPagto.boleto);
                  },
                ),
              ),
              Card(
                color: Layout.light(),
                child: ListTile(
                  leading: CircleAvatar(
                    child: Icon(FontAwesomeIcons.creditCard),
                  ),
                  title: Text('Cartão de Crédito'),
                  subtitle: Text('Liberação imediata'),
                  trailing: Radio(
                    value: TipoPagto.cartao,
                    groupValue: _tipoPagto,
                    activeColor: Colors.blue[900],
                    onChanged: (newVal) => null,
                  ),
                  onTap: () {
                    if (_loading) {
                      return;
                    }
                    setState(() => _tipoPagto = TipoPagto.cartao);
                  },
                ),
              ),
            ],
          ),
        ),
        Expanded(child: SizedBox()),
        Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              _loading
                  ? LinearProgressIndicator(
                      backgroundColor: Colors.blue[700],
                    )
                  : SizedBox(),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  onPressed: _loading
                      ? null
                      : () async {
                          if (_tipoPagto == null) {
                            snackBarDanger('Selecione o método de pagamento');
                            return;
                          }

                          setState(() {
                            _loading = true;
                          });

                          // Verifica se esta tudo certo para salvar a compra
                          if (!(await finalizaController.isValid())) {
                            Navigator.of(context).pop();
                            return;
                          }

                          // Salva efetivamente a compra no Firebase
                          var compraRef =
                              await finalizaController.save(_tipoPagto);

                          // Reset no objeto carrinho
                          carrinho.limpar();

                          // Remove rotas anteriores
                          Navigator.of(context).popUntil(
                            (route) => route.isFirst,
                          );

                          // Puxa a pagina dos detalhes da compra
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) {
                              return CompraDetalhePage(compraRef);
                            },
                          ));
                        },
                  color: Colors.blue[900],
                  disabledColor: Colors.blue[700],
                  textColor: Layout.light(),
                  disabledTextColor: Layout.light(.6),
                  child: Text(_loading ? 'Quase seu...' : 'Finalizar Compra'),
                ),
              ),
            ],
          ),
        )
      ],
    );

    return Layout.render(context, content);
  }
}
