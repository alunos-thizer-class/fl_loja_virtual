import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/categoria_model.dart';
import 'package:fl_loja_virtual/view/home/destaques.dart';
import 'package:fl_loja_virtual/view/home/promo_banners.dart';
import 'package:fl_loja_virtual/view/home/roda_categoria.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  static String tag = '/home-page';

  @override
  Widget build(BuildContext context) {
    var content = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: PromoBanners(),
        ),
        SizedBox(height: 20),
        Expanded(
          flex: 2,
          child: HomeDestaques(),
        ),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 30),
          child: Text(
            'Categorias',
            style: Theme.of(context).textTheme.headline6.copyWith(
                  color: Layout.light(),
                ),
          ),
        ),
        Container(
          height: 90,
          child: SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: FutureBuilder(
              future: buscaListaCategorias(),
              builder: (
                BuildContext context,
                AsyncSnapshot<List<CategoriaModel>> snapshot,
              ) {
                if (snapshot.data == null) {
                  return Center(child: CircularProgressIndicator());
                }

                return RodaCategoria(snapshot.data);
              },
            ),
          ),
        ),
      ],
    );

    return Layout.render(
      context,
      content,
      bottomItemSelected: 0,
    );
  }

  Future<List<CategoriaModel>> buscaListaCategorias() async {
    var result = <CategoriaModel>[];

    var query = await Firestore.instance
        .collection('categoria')
        .where('excluido', isEqualTo: false)
        .getDocuments();

    for (var doc in query.documents) {
      var item = CategoriaModel.fromDocument(doc);
      result.add(item);
    }

    return result;
  }
}
