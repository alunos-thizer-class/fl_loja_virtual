import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/model/banner_model.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class PromoBanners extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: buscaListaDeBanners(),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<BannerModel>> snapshot,
      ) {
        if (snapshot.data == null) {
          return Center(child: CircularProgressIndicator());
        }

        if (snapshot.data.isEmpty) {
          return Center(
            child: Container(
              decoration: BoxDecoration(
                color: Layout.secondaryLight(.9),
                borderRadius: BorderRadius.circular(25),
              ),
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              width: double.infinity,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25),
                child: Image.asset(
                  'assets/images/logo-sem-fundo.png',
                  fit: BoxFit.contain,
                ),
              ),
            ),
          );
        }

        return Swiper(
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int i) {
            var item = snapshot.data[i];

            return Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(25),
                child: Image.network(
                  item.urlImagem,
                  fit: BoxFit.cover,
                ),
              ),
              foregroundDecoration: BoxDecoration(
                border: Border.all(
                  width: 3,
                  color: Layout.light(.8),
                ),
                borderRadius: BorderRadius.circular(25),
              ),
            );
          },
          autoplay: (snapshot.data.length > 1),
          duration: 700,
          // https://api.flutter.dev/flutter/animation/Curves-class.html
          curve: Curves.easeInOutBack,
          pagination: new SwiperPagination(),
          scale: .9,
          viewportFraction: 0.85,
          // layout: SwiperLayout.TINDER,
          // itemWidth: MediaQuery.of(context).size.width - 40,
          // itemHeight: 200,
        );
      },
    );
  }

  Future<List<BannerModel>> buscaListaDeBanners() async {
    var result = <BannerModel>[];

    var banners = await Firestore.instance
        .collection('banner')
        .where('excluido', isEqualTo: false)
        .getDocuments();

    // Ate o momento, aninhar filtros desse modo nao funciona
    // .where('data_inicial', isLessThanOrEqualTo: Timestamp.now())
    // .where('data_final', isGreaterThanOrEqualTo: DateTime.now());

    for (var bannerDoc in banners.documents) {
      var banner = BannerModel.fromDocument(bannerDoc);

      // -1 aqui significa que [banner.dataInicial]
      // eh uma data anterior a agora.
      //
      // exemplo do que retornaria -1:
      // banner.dataInicial = 20/06/20
      // now = 21/06/20
      if (banner.dataInicial.compareTo(Timestamp.now()) != -1) {
        continue; // proximo item do loop
      }

      // 1 aqui significa que [banner.dataFinal]
      // eh uma data posterior a agora.
      //
      // exemplo do que retornaria 1:
      // banner.dataInicial = 21/06/20
      // now = 20/06/20
      if (banner.dataFinal.compareTo(Timestamp.now()) != 1) {
        continue; // proximo item do loop
      }

      result.add(banner);
    }
    return result;
  }
}
