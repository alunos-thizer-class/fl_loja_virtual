import 'dart:math';

import 'package:fl_loja_virtual/model/categoria_model.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/view/produto/categoria_page.dart';
import 'package:flutter/material.dart';

// Identificar o lado que a roda deve girar
enum SwypeDirection { left, right }

class RodaCategoria extends StatefulWidget {
  RodaCategoria(this.categorias);

  final List<CategoriaModel> categorias;

  @override
  _RodaCategoriaState createState() => _RodaCategoriaState();
}

class _RodaCategoriaState extends State<RodaCategoria>
    with SingleTickerProviderStateMixin {
  //
  // Controller da animacao
  AnimationController _controller;

  // para tween 1 = 360º
  // para Transform.rotate = pi * 2 = 360º

  // Controlar o grau de giro por item
  double _startDeg = 0.0;
  double _endDeg = 0.0;

  // Controle do lado que o usuario esta arrastando
  double _dragInitial = 0;
  SwypeDirection _swypeDirection;

  // Controle do item atual
  int _currentItem = 0;

  @override
  void initState() {
    super.initState();

    // Inicia a animacao
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          //
          // Este bloco serve somente de sombra
          //
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(top: 10, bottom: 10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                blurRadius: 3,
                spreadRadius: 2,
                offset: Offset(2, 0),
                color: Layout.dark(.3),
              )
            ],
          ),
        ),
        RotationTransition(
          //
          // Este eh o elemento que realmente eh rotacionado
          //
          turns: Tween(begin: _startDeg, end: _endDeg).animate(_controller),
          child: GestureDetector(
            onTap: () {
              // Item atual (item do topo)
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) =>
                      CategoriaPage(widget.categorias[_currentItem]),
                ),
              );
            },
            onHorizontalDragStart: (details) {
              _dragInitial = details.globalPosition.dx;
            },
            onHorizontalDragUpdate: (details) {
              // Verificamos se arrastou para direita ou esquerda
              _swypeDirection = SwypeDirection.right;

              if ((details.globalPosition.dx - _dragInitial) < 0) {
                _swypeDirection = SwypeDirection.left;
              }
            },
            onHorizontalDragEnd: (details) {
              // Aplica animacao dependendo do lado que arrastou

              // Marca a posicao inicial da roda com a
              // ultima posicao que a animacao fez
              _startDeg = _endDeg;

              // Reinicia a animacao
              _controller.reset();

              switch (_swypeDirection) {
                case SwypeDirection.left:

                  // Informa o angulo para girar
                  _endDeg -= (1 / widget.categorias.length);

                  // Troca o indice do item selecionado (item do topo)
                  _currentItem++;
                  if (_currentItem > widget.categorias.length - 1) {
                    _currentItem = 0;
                  }

                  break;
                case SwypeDirection.right:
                  _endDeg += (1 / widget.categorias.length);

                  _currentItem--;
                  if (_currentItem < 0) {
                    _currentItem = widget.categorias.length - 1;
                  }

                  break;
                default:
              }

              _swypeDirection = null;

              // Dispara a animacao
              setState(() {
                _controller.forward();
              });
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 10, bottom: 10),
              decoration: BoxDecoration(
                color: Layout.secondaryDark(),
                shape: BoxShape.circle,
              ),
              child: Stack(children: _getCategorias()),
            ),
          ),
        ),
      ],
    );
  }

  List<Widget> _getCategorias() {
    List<Widget> result = [];

    result.add(
      ClipRRect(
        //
        // Aqui temos a imagem de "fundo" da roda
        //
        borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width),
        child: Image.asset(
          'assets/images/bg-catwheel.png',
          fit: BoxFit.cover,
        ),
      ),
    );

    // Define o fator de angulacao de cada item
    // Ou seja, o quanto cada um vai ser angulado
    var angleFactor = (pi * 2) / widget.categorias.length;
    var angle = -angleFactor;

    for (CategoriaModel item in widget.categorias) {
      // Aplica fator de angulacao
      angle += angleFactor;

      result.add(
        Transform.rotate(
          //
          // Cada um dos itens da roda
          //
          angle: angle,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Icon(
                    item.getIcone(),
                    color: Layout.light(),
                    size: 32,
                  ),
                ),
                Text(
                  item.nome,
                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                        color: Layout.light(),
                      ),
                )
              ],
            ),
          ),
        ),
      );
    }

    return result;
  }
}
