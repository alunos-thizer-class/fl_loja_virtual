import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_loja_virtual/controller/user_controller.dart';
import 'package:fl_loja_virtual/model/favorito_model.dart';
import 'package:fl_loja_virtual/model/produto_model.dart';
import 'package:fl_loja_virtual/view/layout.dart';
import 'package:fl_loja_virtual/view/produto/produto_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fl_loja_virtual/functions.dart';
import 'package:provider/provider.dart';

class HomeDestaques extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userController = Provider.of<UserController>(context);

    return FutureBuilder(
      future: buscaListaProdutos(),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<ProdutoModel>> snapshot,
      ) {
        if (snapshot.data == null) {
          return Center(child: CircularProgressIndicator());
        }

        if (snapshot.data.isEmpty) {
          return Center(child: Text('Nenhum destaque'));
        }

        return ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: PageScrollPhysics(),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int i) {
            var item = snapshot.data[i];

            return GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ProdutoPage(item.docRef),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                width: MediaQuery.of(context).size.width - 40,
                decoration: BoxDecoration(
                  color: Layout.secondaryLight(.9),
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ClipRRect(
                        child: Stack(
                          children: <Widget>[
                            Image.network(
                              item.imagem,
                              fit: BoxFit.cover,
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width - 40,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Align(
                                alignment: Alignment.topRight,
                                child: StreamBuilder(
                                  stream: Firestore.instance
                                      .collection('favorito')
                                      // .where('excluido', isEqualTo: false)
                                      .where('fk_produto',
                                          isEqualTo: item.docRef)
                                      .where('uid',
                                          isEqualTo: userController.user.uid)
                                      .snapshots(),
                                  builder: (
                                    BuildContext context,
                                    AsyncSnapshot<QuerySnapshot> snapshot,
                                  ) {
                                    if (snapshot.data == null) {
                                      return SizedBox.shrink();
                                    }

                                    FavoritoModel favorito;
                                    if (snapshot.data.documents.isNotEmpty) {
                                      favorito = FavoritoModel.fromDocument(
                                        snapshot.data.documents.first,
                                      );
                                    }

                                    return FlatButton(
                                      child: FaIcon(
                                        (favorito != null &&
                                                favorito.excluido == false)
                                            ? FontAwesomeIcons.solidHeart
                                            : FontAwesomeIcons.heart,
                                        color: Layout.light(),
                                      ),
                                      color: Colors.red[300],
                                      shape: CircleBorder(),
                                      onPressed: () => toggleFavorito(
                                        favorito,
                                        item,
                                        userController.user.uid,
                                      ),
                                      padding: EdgeInsets.all(10),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            item.titulo,
                            style:
                                Theme.of(context).textTheme.headline6.copyWith(
                                      color: Layout.dark(),
                                    ),
                          ),
                          Text(
                            item.chamada,
                            style:
                                Theme.of(context).textTheme.subtitle2.copyWith(
                                      color: Layout.secondaryDark(),
                                    ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              item.preco.toBRL(),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                    color: Layout.primary(),
                                    fontSize: 28,
                                  ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  Future<List<ProdutoModel>> buscaListaProdutos() async {
    var produtos = <ProdutoModel>[];

    var query = await Firestore.instance
        .collection('produto')
        .where('excluido', isEqualTo: false)
        .where('destaque', isEqualTo: true)
        .getDocuments();

    for (var docProd in query.documents) {
      var produto = ProdutoModel.fromDocument(docProd);

      produtos.add(produto);
    }

    return produtos;
  }

  toggleFavorito(FavoritoModel favorito, ProdutoModel item, String uid) {
    // Update ou Insert na model gera uma modificacao
    // no banco e isso eh um gatilho no Stream.
    // Então o efeito sera automatico caso esteja
    // usando StreamBuilder

    if (favorito == null) {
      FavoritoModel(
        fkProduto: item,
        uid: uid,
      ).insert();
    } else {
      favorito.excluido = !favorito.excluido;
      favorito.update();
    }
  }
}
